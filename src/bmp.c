

#include <stdio.h>
#include <stdint.h>
#include <malloc.h>
#include "app.h"
#include "bmp.h"

#pragma pack(1)
struct
	bmpHeader {
	uint16_t bfType;
	uint32_t  bfileSize;
	uint32_t bfReserved; //бесполезно
	uint32_t bOffBits;
	uint32_t biSize;
	uint32_t biWidth;
	uint32_t biHeight;
	uint16_t biPlanes;
	uint16_t biBitCount;
	uint32_t biCompression;
	uint32_t biSizeImage;
	uint32_t biXPelsPerMeter;
	uint32_t biYPelsPerMeter;
	uint32_t biClrUsed;
	uint32_t biClrImportant;
};

enum readStatus fBmp(FILE* in, struct image* img) {
	if (!in) {
		return READ_NO_SUCH_FILE;
	}

	struct bmpHeader header = (struct bmpHeader){ 0 };

	if (fread_s(&header, sizeof(struct bmpHeader), sizeof(struct bmpHeader), 1, in) != 1) return READ_INVALID_HEADER;

	img->height = header.biHeight;
	img->width = header.biWidth;

	struct pixel* data = malloc(sizeof(struct pixel) * header.biWidth * header.biHeight);
	int8_t* trashbean[4];
	for (size_t i = 0; i < header.biHeight; i++) {
		if (fread_s(data + i * header.biWidth, (sizeof(struct pixel) * header.biWidth * header.biHeight) - i * header.biWidth, sizeof(struct pixel), header.biWidth,
			in) == 0) return READ_UNEXPECTED_END_OF_FILE;

		size_t count = (4 - header.biWidth * 3 % 4) % 4;
		if (fread_s(trashbean, (sizeof(int8_t) * 4), sizeof(uint8_t), count, in) == 0 && count != 0) return READ_UNEXPECTED_END_OF_FILE;
	}
	img->data = data;
	return READ_OK;

}


struct bmpHeader  newHeader(FILE* out, struct image const* img) {
	struct bmpHeader header = (struct bmpHeader){
			.bfType = 0x4D42,
			.bfileSize = img->width * img->height + 54 + img->width % 4 * img->height,
			.bfReserved = 0,
			.bOffBits = 54,
			.biSize = 40,
			.biWidth = img->width,
			.biHeight = img->height,
			.biPlanes = 1,
			.biBitCount = 24,
			.biCompression = 0,
			.biSizeImage = img->height * (img->width * sizeof(struct pixel) + img->width % 4),
			.biXPelsPerMeter = 0,
			.biYPelsPerMeter = 0,
			.biClrUsed = 0,
			.biClrImportant = 0,
	};
	return header;
}

enum writeStatus toBmp(FILE* out, struct image const* bmp) {
	struct bmpHeader header = newHeader(out, bmp);
	fwrite(&header, sizeof(struct bmpHeader), 1, out);
	uint8_t trash = { 0 };
	for (size_t i = 0; i < header.biHeight; i++) {
		fwrite(bmp->data + i * header.biWidth, sizeof(struct pixel), header.biWidth, out);
		if (header.biWidth % 4 != 0)
			for (size_t t = 0; t < 4 - header.biWidth * 3 % 4; t++)
				fwrite(&trash, sizeof(uint8_t), 1, out);
	}
	return READ_OK;
}
