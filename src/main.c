#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdint.h>
#include "app.h"
#include "bmp.h"
#include "util.h"

void usage() {
	fprintf(stderr, "Usage: ./print_header BMP_FILE_NAME\n");
}

int main(int args, char** arg) {
	if (args != 3) usage();
	if (args < 3) err("Not enough arguments \n");

	struct image img = { 0 };
	struct image new = { 0 };
	fprintf(stderr, "File reading finished with exit code %d\n", readFile(arg[1], &img));
	int typeRot = 0;
	printf("input number( if(Number>0)-rotate left else rotate right):");
	scanf("%d", &typeRot);
	if (typeRot > 0) {
		new = rotL(img, typeRot);
	}
	else
	{
		new = rotR(img, typeRot);
	}
	fprintf(stderr, "File writing finished with exit code %d\n", writeFile(arg[2], &new));
	return 0;
}





