#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>
#include <malloc.h>
#include "app.h"
#include "bmp.h"

struct image rotR(const struct image  origImg) {
	struct pixel* pixels = malloc(origImg.height * origImg.width * sizeof(struct pixel));
	int i = 0;
	uint64_t colum, row, newColumn, newRow;
	while (i < origImg.height * origImg.width) {
		colum = i % origImg.width;
		row = i / origImg.width;
		newColumn = row;
		newRow = origImg.width - colum - 1;
		pixels[newRow * origImg.height + newColumn] = origImg.data[i];
		i++;
	}
	struct image rotated = { origImg.height, origImg.width, pixels };
	return rotated;
}
struct image rotL(const struct image  origImg) {
	struct pixel* pixels = malloc(origImg.height * origImg.width * sizeof(struct pixel));
	int i = 0;
	uint64_t colum,row, newColumn, newRow;
	while (i < origImg.height * origImg.width) {
		colum = i % origImg.width;
		row = i / origImg.width;
		newColumn = origImg.height - row - 1;
		newRow = colum;
		pixels[newRow * origImg.height + newColumn] = origImg.data[i];
		i++;
	}
	struct image rotated = { origImg.height, origImg.width, pixels };
	return rotated;
}


enum readStatus readFile(const char* name, struct image const* im) {
	FILE* input;
	if (fopen_s(&input, name, "r+b")) return READ_CANNOT_OPEN_FILE;
	enum readStatus result = fBmp(input, im);
	if (input) fclose(input);
	return result;
}

enum writeStatus writeFile(const char* name, struct image const* im) {
	FILE* output = NULL;
	if (fopen_s(&output, name, "w+b")) return READ_CANNOT_OPEN_FILE;
	enum readStatus result = toBmp(output, im);
	if (output) {
		fclose(output);
	}
	return result;
}


