#ifndef BMP_H
#define BMP_H

enum readStatus fBmp( FILE* in, struct image* img );
enum writeStatus toBmp( FILE* out, struct image const* img );

#endif 
