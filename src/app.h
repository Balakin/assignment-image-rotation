#ifndef APP_H
#define APP_H

struct pixel { uint8_t b, g, r; };
struct image {
	uint64_t width;
	uint64_t height;
	struct pixel* data;
};


struct image rotR(struct image const source);
struct image rotL(struct image const source);
void printImg(struct image const im);

enum readStatus {
	READ_OK,
	READ_NO_SUCH_FILE,
	READ_INVALID_BITS,
	READ_INVALID_HEADER,
	READ_CANNOT_OPEN_FILE,
	READ_UNEXPECTED_END_OF_FILE
	// коды ошибок  
};
enum  writeStatus {
	WRITE_OK = 0,
	WRITE_ERROR
	// другие ошибки  
};

enum readStatus readFile(const char* name, struct image const* im);
enum writeStatus writeFile(const char* name, struct image const* im);


#endif 
